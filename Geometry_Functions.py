import numpy as np
import geopy.distance as distance
from shapely.geometry import Point, Polygon
# from geojson.geometry import Point, Polygon
import math
import json


def Point_Observed(object_loc, observed_polygon):
    boundary = json.loads(str(observed_polygon))
    observed_area = Polygon(boundary)
    Scenario_Object = Point(object_loc)
    observed = Scenario_Object.within(observed_area)
    return observed


def get_random_point_in_polygon(observed_polygon):
    boundary = json.loads(str(observed_polygon))
    observed_area = Polygon(boundary)
    minx, miny, maxx, maxy = observed_area.bounds
    while True:
        p = Point(np.random.uniform(minx, maxx), np.random.uniform(miny, maxy))
        if observed_area.contains(p):
            return p


def GreatCircleBearing(lon1, lat1, lon2, lat2):
    d2r = np.pi/180
    dLong = lon1 - lon2

    s = math.cos(d2r*lat2)*math.sin(d2r*dLong)
    c = math.cos(d2r*lat1)*math.sin(d2r*lat2) - math.sin(lat1*d2r)*math.cos(d2r*lat2)*math.cos(d2r*dLong)

    return np.arctan2(s, c)


def get_Polygon_Area(lats, lons):
    Rad_Earth_km = 6371
    N = len(lons)

    angles = np.empty(N)
    for i in range(N):
        phiB1, phiA, phiB2 = np.roll(lats, i)[:3]
        LB1, LA, LB2 = np.roll(lons, i)[:3]

        # calculate angle with north (eastward)
        beta1 = GreatCircleBearing(LA, phiA, LB1, phiB1)
        beta2 = GreatCircleBearing(LA, phiA, LB2, phiB2)

        # calculate angle between the polygons and add to angle array
        angles[i] = np.arccos(math.cos(-beta1) * math.cos(-beta2) + math.sin(-beta1) * math.sin(-beta2))

    area = (sum(angles) - (N - 2) * np.pi) * Rad_Earth_km**2

    return area


def get_geodistance(lat1, long1, lat2, long2):
    pos1 = (lat1, long1)
    pos2 = (lat2, long2)
    dist = distance.geodesic(pos1, pos2).kilometers

    return dist


def get_distance_across_earth_lla(lla1, lla2):

    ecfPoint1 = lla2ecf(lla1[0], lla1[1], lla1[2])
    ecfPoint2 = lla2ecf(lla2[0], lla2[1], lla2[2])
    angle = np.arccos(np.dot(ecfPoint1, ecfPoint2) / (np.linalg.norm(ecfPoint1) * np.linalg.norm(ecfPoint2)))
    earthRadius1 = earthRadiusAtLat(lla1[1])
    earthRadius2 = earthRadiusAtLat(lla1[1])
    earthRadiusAve = (earthRadius1 + earthRadius2) / 2
    return angle * earthRadiusAve


def lla2ecf(latDeg, lonDeg, altKM):

    deg2rad = np.pi / 180.0

    EARTH_EQUATORIAL_RADIUS_KM = 6378.137000  # 'a' measurement of an ellipse
    EARTH_FLATTENING = 1 / 298.257223563
    EARTH_POLAR_RADIUS_KM = EARTH_EQUATORIAL_RADIUS_KM * (1 - EARTH_FLATTENING)  # 'b' measurement of an ellipse
    EARTH_ecc_squared = 1 - (EARTH_POLAR_RADIUS_KM ** 2) / (EARTH_EQUATORIAL_RADIUS_KM ** 2)

    sinlat = np.sin(latDeg * deg2rad)
    coslat = np.cos(latDeg * deg2rad)
    sinlon = np.sin(lonDeg * deg2rad)
    coslon = np.cos(lonDeg * deg2rad)

    primeVerticalRadius = EARTH_EQUATORIAL_RADIUS_KM / np.sqrt((1 - EARTH_ecc_squared * sinlat * sinlat))

    positionX = ((primeVerticalRadius + altKM) * coslat * coslon)
    positionY = ((primeVerticalRadius + altKM) * coslat * sinlon)
    positionZ = (((primeVerticalRadius * (1.0 - EARTH_ecc_squared)) + altKM) * sinlat)

    return np.array((positionX, positionY, positionZ))


def ecf2lla(position):

    rad2deg = 180.0/np.pi
    deg2rad = np.pi/180.0

    EARTH_EQUATORIAL_RADIUS_KM = 6378.137000       # 'a' measurement of an ellipse
    EARTH_FLATTENING = 1/298.257223563
    EARTH_POLAR_RADIUS_KM = EARTH_EQUATORIAL_RADIUS_KM*(1-EARTH_FLATTENING)   # 'b' measurement of an ellipse
    EARTH_ecc_squared = 1 - (EARTH_POLAR_RADIUS_KM**2)/(EARTH_EQUATORIAL_RADIUS_KM**2)
    EARTH_ONE_MINUS_eccsq = 1.0 - EARTH_ecc_squared

    lon = rad2deg * np.arctan2(position[1], position[0])
    if lon > 180:
        lon = lon - 360

    p = np.sqrt((position[0] * position[0]) + (position[1] * position[1]))
    if p < 0.1:
        p = 0.1

    q = position[2] / p

    alt = 0.0
    lat = rad2deg * np.arctan(q * (1.0 / EARTH_ONE_MINUS_eccsq))

    a = 1.00
    i = 0
    # d = 0.00
    # primeVerticalRadius = 0.00
    # b = 0.00

    while a > 0.002 or i < 300:
        sinlat = np.sin(lat * deg2rad)
        sinlat2 = sinlat * sinlat
        primeVerticalRadius = EARTH_EQUATORIAL_RADIUS_KM/np.sqrt(1 - EARTH_ecc_squared*sinlat2)
        d = alt
        alt = (p / np.cos(lat * deg2rad)) - primeVerticalRadius
        a = q * (primeVerticalRadius + alt)
        b = (EARTH_ONE_MINUS_eccsq * primeVerticalRadius) + alt
        lat = rad2deg * np.arctan2(a, b)
        a = np.abs(alt - d)
        i = i + 1

    return np.array((lat, lon, alt))


def earthRadiusAtLat(latitude):

    Re = 6378.137
    EARTH_FLATTENING = 1/298.257223563
    Rp = Re*(1-EARTH_FLATTENING)
    latRad = latitude * np.pi/180.0

    num = (Re*Re*np.cos(latRad))**2.000 + (Rp*Rp*np.sin(latRad))**2.000
    denom = (Re*np.cos(latRad))**2.000 + (Rp*np.sin(latRad))**2.000

    radius = np.sqrt(num/denom)
    return radius
