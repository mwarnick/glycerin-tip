import numpy as np
import SpaceLynx_Functions


def Generate_Tips(detections_df, Sch_ID, MDV_m_per_sec, Track_Revisit_sec, Processing_Delay_sec, Sim_End):

    TP_FP_detections_df = detections_df[detections_df['Detection_Type'] == 'TP']
    TP_FP_detections_df = TP_FP_detections_df[np.abs(TP_FP_detections_df['Radial_Velocity']) > MDV_m_per_sec*3.6]
    TP_FP_detections_df = TP_FP_detections_df[~TP_FP_detections_df['Target_Name'].str.contains('tip')]
    TP_FP_detections_df = TP_FP_detections_df.sort_values('Time_sec')

    tip_targets = list()
    target_name_list = list()

    for i in range(len(TP_FP_detections_df)):
        if target_name_list.__contains__(TP_FP_detections_df['Target_Name'].to_list()[i]):
            continue
        target_name_list.append(TP_FP_detections_df['Target_Name'].to_list()[i])
        target = SpaceLynx_Functions.Load_JSON_File('Sample_Inputs/Target_Template.json')
        target['id'] = str(i+100*Sch_ID)
        target['name'] = TP_FP_detections_df['Target_Name'].to_list()[i] + '_tip_' + str(i)
        target['type'] = 'POINT'
        target['target_latitude_deg'] = TP_FP_detections_df['Target_Latitude'].to_list()[i]
        target['target_longitude_deg'] = TP_FP_detections_df['Target_Longitude'].to_list()[i]
        target['target_altitude_km'] = 0
        target['target_length_km'] = 5
        target['target_width_km'] = 5
        target['target_orientation_deg'] = 0
        target['priority'] = 1
        target['lock_type'] = 'UNLOCKED'
        target['target_revisit_min'] = Track_Revisit_sec/60
        target['description_a'] = 'Track'
        target['description_b'] = TP_FP_detections_df['Target_Name'].to_list()[i]
        target['constraints'][0]['phenomenology'] = 'SAR_SPOTLIGHT'
        target['constraints'][0]['common_countdown'] = 'UNIQUE'
        # target['constraints'][0]['acceptable_payload_names'] = ''
        target['constraints'][0]['access_constraints']['eclipse_operation'] = 'No Constraint'
        target['constraints'][0]['access_constraints']['min_niirs'] = 3.0
        target['constraints'][0]['phenomenology_parameters']['type_id'] = 'BLNX_SAR_PARAMETERS'

        target_start_interval = TP_FP_detections_df['Time'].to_list()[i] + np.timedelta64(Processing_Delay_sec, 's')
        target_start_interval = str(target_start_interval.date()) + 'T' + str(target_start_interval.time())[:8] + 'Z'
        target["valid_intervals"] = [{'start': target_start_interval, 'end': Sim_End}]

        tip_targets.append(target)

    return tip_targets
