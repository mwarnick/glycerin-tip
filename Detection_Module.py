import Geometry_Functions as GF
import Ground_Truth_Module as Truth
import numpy as np
import random
import sys
import pandas as pd
import datetime
# import json
import Data_Classes as Classes
# from dacite import from_dict


def get_Ground_Truth_Data(scenario_data, time):

    Target_Data_List = []
    Target_List = scenario_data['Actions'].Target_Name.unique().tolist()
    for target in Target_List:
        Target_Data = Truth.Action(scenario_data, target, time)
        Target_Data_List.append(Target_Data)

    return Target_Data_List


# def get_Collection_Schedule(json_schedule_file):
#     schedules = populate_OutputData(json_schedule_file)
#     return schedules

# def get_Collection_Schedule(thisInput):
#     if type(thisInput) == str:
#         with open(thisInput) as json_file:
#             data = json.loads(thisInput)
#         scheduleResponse = from_dict(data_class=Classes.Response, data=data)
#     else:
#         scheduleResponse = thisInput
#
#     thisOutput = []
#     for thisAsset in scheduleResponse.schedule:
#         thisAsset = scheduleResponse.schedule[thisAsset]
#         for thisOp in thisAsset.ops:
#             thisOutput.append(Classes.OutputData(thisOp))
#     return thisOutput


def get_Detections(collection_data, ground_truth):
    i = 0
    Detected_Targets = []
    Detection_Output_Columns = ['Time', 'Time_sec', 'Asset_Name', 'Payload_Name', 'Target_Name', 'Target_Collected',
                                'Target_Visible', 'Target_Active', 'Target_Type', 'Target_State', 'Target_Latitude',
                                'Target_Longitude', 'Phenomenology', 'Quality', 'Detection_Type', 'Total_Area_Collected',
                                'PDetect', 'FAR', 'Radial_Velocity']

    for Collect in collection_data:

        total_collects = len(collection_data)
        i += 1
        if i % 5 == 0:
            print('Collect Number ', i, ' of ', total_collects)
        if 'tip' in Collect.Target_Name:
            continue

        Collect_Time = (Collect.Start_Time_sec + Collect.End_Time_sec) / 2
        Collect_Area = Collect.Target_Area_sq_km

        collect_coord = Collect.geojson
        if collect_coord['type'] == 'Polygon':
            collect_polygon = collect_coord['coordinates'][0]
        elif collect_coord['type'] == 'Point':
            long = collect_coord['coordinates'][0]
            lat = collect_coord['coordinates'][1]
            collect_polygon = [[long-1/(60*np.cos(lat*np.pi/180)), lat-1/60, 0], [long-1/(60*np.cos(lat*np.pi/180)), lat+1/60, 0],
                               [long+1/(60*np.cos(lat*np.pi/180)), lat+1/60, 0], [long+1/(60*np.cos(lat*np.pi/180)), lat-1/60, 0],
                               [long-1/(60*np.cos(lat*np.pi/180)), lat-1/60, 0]]
        else:
            sys.exit('Shape Type not Supported')

        Target_Data = get_Ground_Truth_Data(ground_truth, Collect_Time)

        for targets in Target_Data:

            Detection_Data = Classes.Detection()
            Detection_Data.Time = Collect.Start_Time + datetime.timedelta(seconds=(Collect.End_Time_sec - Collect.Start_Time_sec) / 2)
            Detection_Data.Time_sec = Collect_Time
            Detection_Data.Asset_Name = Collect.Asset_Name
            Detection_Data.Payload_Name = Collect.Payload_Name
            Detection_Data.Target_Name = targets.Target_Name
            Detection_Data.Target_Collected = GF.Point_Observed([targets.Longitude_deg, targets.Latitude_deg], collect_polygon)
            Detection_Data.Target_Visible = targets.Visible
            Detection_Data.Target_Active = targets.Target_Active
            Detection_Data.Target_Type = targets.Target_Type
            Detection_Data.Target_State = targets.Target_State
            Detection_Data.Target_Latitude = targets.Latitude_deg
            Detection_Data.Target_Longitude = targets.Longitude_deg
            Detection_Data.Phenomenology = Collect.Phenomenology
            Detection_Data.Quality = Collect.NIIRS
            Detection_Data.Total_Area_Collected = Collect_Area
            Detection_Data.PDetect = get_Prob_Det(targets)
            Detection_Data.FAR = get_FAR(targets)
            Detection_Data.Radial_Velocity = get_Radial_Velocity(targets, Collect)

            if not Detection_Data.Target_Collected:
                Detection_Data.Detection_Type = 'Not_Collected'
            elif not Detection_Data.Target_Active:
                Detection_Data.Detection_Type = 'Not_Active'
            elif not Detection_Data.Target_Visible:
                Detection_Data.Detection_Type = 'Not_Visible'
            else:
                random_num = random.random()
                if random_num < Detection_Data.PDetect:
                    Detection_Data.Detection_Type = 'TP'
                else:
                    Detection_Data.Detection_Type = 'FN'

            Detection_Data_dict = Detection_Data.__dict__
            Detected_Targets.append(Detection_Data_dict)

        for j in range(len(ground_truth['Target'])):
            target_attributes = ground_truth['Target'].iloc[j]
            FAR = get_FAR(target_attributes)
            expected_FA = FAR * Collect_Area
            num_FA = np.random.poisson(expected_FA, 1)
            for k in range(num_FA[0]):
                FA_Point = GF.get_random_point_in_polygon(collect_polygon)

                Detection_Data = Classes.Detection()
                Detection_Data.Time = Collect.Start_Time + datetime.timedelta((Collect.End_Time_sec - Collect.Start_Time_sec) / 2)
                Detection_Data.Time_sec = Collect_Time
                Detection_Data.Asset_Name = Collect.Asset_Name
                Detection_Data.Payload_Name = Collect.Payload_Name
                Detection_Data.Target_Name = 'FALSE_ALARM'
                Detection_Data.Target_Collected = True
                Detection_Data.Target_Visible = True
                Detection_Data.Target_Active = True
                Detection_Data.Target_Type = target_attributes.Target_Type
                Detection_Data.Target_State = target_attributes.Target_State
                Detection_Data.Target_Latitude = FA_Point.y
                Detection_Data.Target_Longitude = FA_Point.x
                Detection_Data.Phenomenology = Collect.Phenomenology
                Detection_Data.Quality = Collect.NIIRS
                Detection_Data.Total_Area_Collected = Collect_Area
                Detection_Data.PDetect = get_Prob_Det(target_attributes)
                Detection_Data.FAR = FAR
                Detection_Data.Radial_Velocity = -9999
                Detection_Data.Detection_Type = 'FP'
                Detection_Data_dict = Detection_Data.__dict__
                Detected_Targets.append(Detection_Data_dict)

    Detected_Targets_df = pd.DataFrame(Detected_Targets, columns=Detection_Output_Columns)
    return Detected_Targets_df


def get_Prob_Det(target_data):
    if target_data.Target_State == 'StoppedVisible':
        return 0
    elif target_data.Target_State == 'Moving_Medium':
        return .80
    else:
        return .95


def get_FAR(target_data):
    if target_data.Target_State == 'Stopped_Visible' or target_data.Target_State == 'Stopped_NotVis':
        return 0
    elif target_data.Target_State == 'Moving_Medium':
        return .005
    else:
        return .001


def get_Radial_Velocity(target_data, asset_data):
    target_vel = np.array([target_data.Velocity_ECEF_X_km, target_data.Velocity_ECEF_Y_km, target_data.Velocity_ECEF_Z_km])
    target_pos = np.array([target_data.Position_ECEF_X_km, target_data.Position_ECEF_Y_km, target_data.Position_ECEF_Z_km])
    asset_pos = np.array(asset_data.Pos_ECF)
    # asset_pos = np.array(json.loads(str(asset_pos)))     # Changed here - commented out
    T2A = asset_pos/1000 - target_pos
    T2A_unit = T2A / np.linalg.norm(T2A)
    rad_velocity = np.dot(T2A_unit, target_vel)

    return rad_velocity
