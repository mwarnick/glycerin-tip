import pandas as pd
import numpy as np
from scipy import interpolate
import sys
import bisect
import Geometry_Functions as GF


def Import_Scenario_Data(scenario_file):

    Location_df = pd.read_excel(scenario_file, sheet_name='Fixed_Locations')
    Target_df = pd.read_excel(scenario_file, sheet_name='Target_Attributes')
    Reflectance_df = pd.read_excel(scenario_file, sheet_name='Spectral_Ref')
    Path_Definition_df = pd.read_excel(scenario_file, sheet_name='Path_Definition')
    Path_Points_df = pd.read_excel(scenario_file, sheet_name='Path_Points')
    Actions_df = pd.read_excel(scenario_file, sheet_name='Actions')

    Scenario_Data = {'Location': Location_df, 'Target': Target_df, 'Reflectance': Reflectance_df,
                     'Path_Def': Path_Definition_df, 'Path_Points': Path_Points_df, 'Actions': Actions_df}

    return Scenario_Data


class Path:

    def __init__(self, scenario_data, path_name):
        path_data = scenario_data['Path_Def']
        path_points = scenario_data['Path_Points']
        Path_Definition = path_data[(path_data['Path_Name'] == path_name)]

        Lat_Points = path_points[path_name + '_Lat_deg'].tolist()
        Long_Points = path_points[path_name + '_Lon_deg'].tolist()
        Alt_Points = path_points[path_name + '_Alt_km'].tolist()

        Lat_Points = [i for i in Lat_Points if i == i]
        Long_Points = [i for i in Long_Points if i == i]
        Alt_Points = [i for i in Alt_Points if i == i]

        self.Path_Name = Path_Definition.Path_Name.to_list()[0]
        self.Start_Loc_Name = Path_Definition.Start_Location.to_list()[0]
        self.End_Loc_name = Path_Definition.End_Location.to_list()[0]
        self.Path_Length_km = Path_Definition.Length_km.to_list()[0]
        self.Lat_Points = Lat_Points
        self.Long_Points = Long_Points
        self.Alt_Points = Alt_Points

    def get_Path_Length(self):
        path_length = 0
        for i in np.arange(len(self.Lat_Points) - 1):
            dist = GF.get_geodistance(self.Lat_Points[i], self.Long_Points[i], self.Lat_Points[i + 1], self.Long_Points[i + 1])
            path_length += dist

        return path_length


class Location(Path):

    def __init__(self, scenario_data, target_name, scenario_time_sec):

        loc_data = scenario_data['Location']
        path_data = scenario_data['Path_Def']
        action_data = scenario_data['Actions']

        Action_Definition = action_data[action_data['Target_Name'] == target_name]
        Start_Time_sec_list = Action_Definition.Start_Time_sec.tolist()
        Duration_list = Action_Definition.Duration_sec.tolist()
        End_Time_sec_list = np.add(Start_Time_sec_list, Duration_list).tolist()
        Speed_km_hr_list = Action_Definition.Speed_km_hr.tolist()
        Position_List = Action_Definition.Position.tolist()

        if scenario_time_sec > End_Time_sec_list[-1] or scenario_time_sec < Start_Time_sec_list[0]:
            self.Latitude_deg = -9999
            self.Longitude_deg = -9999
            self.Altitude_km = -9999
            self.Position_ECEF_X_km = -9999
            self.Position_ECEF_Y_km = -9999
            self.Position_ECEF_Z_km = -9999
            self.Velocity_ECEF_X_km = -9999
            self.Velocity_ECEF_Y_km = -9999
            self.Velocity_ECEF_Z_km = -9999
        else:
            index = bisect.bisect(Start_Time_sec_list, scenario_time_sec) - 1

            Start_Time_sec = Start_Time_sec_list[index]
            Speed_km_hr = Speed_km_hr_list[index]
            Loc_or_Path_Name = Position_List[index]

            if Loc_or_Path_Name in loc_data['Location_Name'].tolist():

                Loc_Data = loc_data.loc[loc_data['Location_Name'] == Loc_or_Path_Name]

                self.Latitude_deg = Loc_Data.Latitude_deg.__float__()
                self.Longitude_deg = Loc_Data.Longitude_deg.__float__()
                self.Altitude_km = Loc_Data.Altitude_km.__float__()
                self.Position_ECEF_X_km = GF.lla2ecf(self.Latitude_deg, self.Longitude_deg, self.Altitude_km)[0]
                self.Position_ECEF_Y_km = GF.lla2ecf(self.Latitude_deg, self.Longitude_deg, self.Altitude_km)[1]
                self.Position_ECEF_Z_km = GF.lla2ecf(self.Latitude_deg, self.Longitude_deg, self.Altitude_km)[2]
                self.Velocity_ECEF_X_km = 0
                self.Velocity_ECEF_Y_km = 0
                self.Velocity_ECEF_Z_km = 0

            elif Loc_or_Path_Name in path_data['Path_Name'].tolist():

                Path.__init__(self, scenario_data, Loc_or_Path_Name)

                Loc_Vel_Data = Location.get_Mover_Location_Velocity(self, scenario_time_sec, Start_Time_sec, Speed_km_hr)

                self.Latitude_deg = Loc_Vel_Data['Position_Lat_deg']
                self.Longitude_deg = Loc_Vel_Data['Position_Long_deg']
                self.Altitude_km = Loc_Vel_Data['Position_Alt_km']
                self.Position_ECEF_X_km = Loc_Vel_Data['Position_ECEF_X_km']
                self.Position_ECEF_Y_km = Loc_Vel_Data['Position_ECEF_Y_km']
                self.Position_ECEF_Z_km = Loc_Vel_Data['Position_ECEF_Z_km']
                self.Velocity_ECEF_X_km = Loc_Vel_Data['Velocity_ECEF_X_km']
                self.Velocity_ECEF_Y_km = Loc_Vel_Data['Velocity_ECEF_Y_km']
                self.Velocity_ECEF_Z_km = Loc_Vel_Data['Velocity_ECEF_Z_km']

            else:
                sys.exit('Location or Path Name not Properly Defined')

    def get_Mover_Location_Velocity(self, sim_time, path_start_time, velocity_km_hr):

        distance_traveled_km = (sim_time - path_start_time) / 3600 * velocity_km_hr

        segment_distances = []

        for i in np.arange(len(self.Lat_Points) - 1):
            # seg_dist = GF.get_geodistance(self.Lat_Points[i], self.Long_Points[i], self.Lat_Points[i + 1], self.Long_Points[i + 1])
            seg_dist = GF.get_distance_across_earth_lla([self.Lat_Points[i], self.Long_Points[i], 0],
                                                        [self.Lat_Points[i+1], self.Long_Points[i+1], 0])
            segment_distances.append(seg_dist)

        cum_segment_dist = np.cumsum(segment_distances).tolist()
        cum_segment_dist.insert(0, 0)
        index = bisect.bisect(cum_segment_dist, distance_traveled_km)
        leftover_dist = distance_traveled_km - cum_segment_dist[index - 1]
        percent = leftover_dist / segment_distances[index - 1]

        Position_Segment_Start_ECEF_km = GF.lla2ecf(self.Lat_Points[index-1], self.Long_Points[index-1], self.Alt_Points[index-1])
        Position_Segment_End_ECEF_km = GF.lla2ecf(self.Lat_Points[index], self.Long_Points[index], self.Alt_Points[index])
        Segment_Vector = np.subtract(Position_Segment_End_ECEF_km, Position_Segment_Start_ECEF_km)
        Velocity_ECEF_km_hr = velocity_km_hr * Segment_Vector / np.linalg.norm(Segment_Vector)

        Position_Latitude_deg = (self.Lat_Points[index] - self.Lat_Points[index - 1]) * percent + self.Lat_Points[index - 1]
        Position_Longitude_deg = (self.Long_Points[index] - self.Long_Points[index - 1]) * percent + self.Long_Points[index - 1]
        Position_Altitude_km = (self.Alt_Points[index] - self.Alt_Points[index - 1]) * percent + self.Alt_Points[index - 1]
        Position_ECEF_km = GF.lla2ecf(Position_Latitude_deg, Position_Longitude_deg, Position_Altitude_km)

        Results_Dict = {'Position_Lat_deg': Position_Latitude_deg, 'Position_Long_deg': Position_Longitude_deg,
                        'Position_Alt_km': Position_Altitude_km, 'Position_ECEF_X_km': Position_ECEF_km[0],
                        'Position_ECEF_Y_km': Position_ECEF_km[1], 'Position_ECEF_Z_km': Position_ECEF_km[2],
                        'Velocity_ECEF_X_km': Velocity_ECEF_km_hr[0], 'Velocity_ECEF_Y_km': Velocity_ECEF_km_hr[1],
                        'Velocity_ECEF_Z_km': Velocity_ECEF_km_hr[2]}

        return Results_Dict


class Spect_Reflectance:

    def __init__(self, scenario_data, target_refl_name, BG_refl_name):
        refl_data = scenario_data['Reflectance']
        self.Wavelength_um = refl_data.Wavelength_um.tolist()
        self.Target_Spect_Refl_perc = refl_data[target_refl_name].tolist()
        self.BG_Spect_Refl_perc = refl_data[BG_refl_name].tolist()

    def get_reflectance(self, wavelength_um, source):
        if source == 'Target':
            Reflectance = interpolate.interp1d(self.Wavelength_um, self.Target_Spect_Refl_perc)
        elif source == 'BG':
            Reflectance = interpolate.interp1d(self.Wavelength_um, self.BG_Spect_Refl_perc)
        else:
            sys.exit('Source not Properly Defined')

        return Reflectance(wavelength_um).__float__()


class Target(Spect_Reflectance):

    def __init__(self, scenario_data, target_type, target_state):

        target_data = scenario_data['Target']
        Target_Data = target_data[(target_data['Target_Type'] == target_type) & (target_data['Target_State'] == target_state)]

        self.Target_Type = target_type
        self.Target_State = target_state
        self.Target_Length_m = Target_Data.Target_Length_m.__float__()
        self.Target_Width_m = Target_Data.Target_Width_m.__float__()
        self.Target_Temp_K = Target_Data.Target_Temp_K.__float__()
        self.Target_Emm_perc = Target_Data.Target_Emissivity_perc.__float__()
        self.Target_RCS_dbsm = Target_Data.Target_RCS_dbsm.__float__()
        self.BG_Temp_K = Target_Data.BG_Temp_K.__float__()
        self.BG_Emm_perc = Target_Data.BG_Emissivity_perc.__float__()
        self.BG_RCS_m2_per_m2 = Target_Data.BG_RCS_m2_per_m2.__float__()

        self.Target_Refl_Name = Target_Data.Target_Spectral_Reflectance_perc.to_list()[0]
        self.BG_Refl_Name = Target_Data.BG_Spectral_Reflectance_perc.to_list()[0]
        Spect_Reflectance.__init__(self, scenario_data, self.Target_Refl_Name, self.BG_Refl_Name)


class Action(Target, Location):

    def __init__(self, scenario_data, target_name, scenario_time_sec):

        action_data = scenario_data['Actions']
        Action_Definition = action_data[action_data['Target_Name'] == target_name]

        Target_Type_List = Action_Definition.Target_Type.tolist()
        Target_State_list = Action_Definition.Target_State.tolist()
        Visible_list = Action_Definition.Visible.tolist()
        Start_Time_sec_list = Action_Definition.Start_Time_sec.tolist()
        Duration_list = Action_Definition.Duration_sec.tolist()
        End_Time_sec_list = np.add(Start_Time_sec_list, Duration_list).tolist()
        Speed_km_hr_list = Action_Definition.Speed_km_hr.tolist()
        Position_List = Action_Definition.Position.tolist()

        index = -1
        if scenario_time_sec > End_Time_sec_list[-1] or scenario_time_sec < Start_Time_sec_list[0]:
            self.Target_Active = False
        else:
            index = bisect.bisect(Start_Time_sec_list, scenario_time_sec) - 1
            self.Target_Active = True

        self.Target_Name = target_name
        self.Scenario_Time_sec = scenario_time_sec
        self.Target_Type = Target_Type_List[index]
        self.Target_State = Target_State_list[index]
        self.Visible = Visible_list[index]
        self.Start_Time_sec = Start_Time_sec_list[index]
        self.Duration_sec = End_Time_sec_list[index]
        self.Speed_km_hr = Speed_km_hr_list[index]
        self.Loc_or_Path_Name = Position_List[index]

        Target.__init__(self, scenario_data, self.Target_Type, self.Target_State)
        Location.__init__(self, scenario_data, self.Target_Name, self.Scenario_Time_sec)
