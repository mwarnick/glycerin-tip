import pandas as pd
import numpy as np

'''
scenarioDefinitionFile = 'C:\SpaceLynx\SpaceLynx 2.0\Scenario\Sample_Inputs\Canadian_Scenario_Definition.xlsx'
opsFile = 'C:\SpaceLynx\SpaceLynx 2.0\Scenario\Output_Files\Ops_All.csv'
trackingCustodyReqSecs = 30
trackingStatsFile = 'C:\SpaceLynx\SpaceLynx 2.0\Scenario\Output_Files\\trackStats.xlsx'
'''

def make_tracking_stats(scenarioDefinitionFile, opsFile, trackingCustodyReqSecs, trackingStatsFile):
    scenarioDF = pd.read_excel(scenarioDefinitionFile, 'Actions')
    opsDF = pd.read_csv(opsFile)
    opsTrackDF = opsDF[opsDF.Target_Desc_A.str.contains('Track')]
    trackingStatsList = []
    trackingStatsHeader = ['Target_Name','Target_State','Speed_km_hr','Position','State_Start_Time_sec','State_End_Time_sec',
                      'Track_Start_Time_sec', 'Track_Delay_sec', 'Tracking_Fraction', 'Track_Duration_sec']

    for thisIndex in scenarioDF.index:
        thisTarget = scenarioDF.loc[thisIndex,'Target_Name']
        thisStartSec = scenarioDF.loc[thisIndex,'Start_Time_sec']
        thisEndSec = thisStartSec + scenarioDF.loc[thisIndex,'Duration_sec']
        thisState = scenarioDF.loc[thisIndex,'Target_State']
        thisPosition = scenarioDF.loc[thisIndex,'Position']
        thisSpeed = scenarioDF.loc[thisIndex,'Speed_km_hr']
        thisTrackingDF = opsTrackDF[(opsTrackDF.Start_Time_sec.ge(thisStartSec)) & 
                                    (opsTrackDF.Start_Time_sec.le(thisEndSec)) &
                                    (opsTrackDF.Target_Desc_B.str.contains(thisTarget))]
        if not thisTrackingDF.empty:
            thisTrackingDF = thisTrackingDF.sort_values(by = ['Start_Time_sec'], ascending=True)
            startTimes = np.array(thisTrackingDF['Start_Time_sec'])
            startTimeDiffs = np.diff(startTimes)
            goodIndexes = startTimeDiffs <= trackingCustodyReqSecs
            thisTrackDuration = np.sum(startTimeDiffs[goodIndexes])
            firstIndex = np.where(goodIndexes==True)[0][0]
            thisFirstTrack = startTimes[firstIndex]
            thisFirstTrackDelay = thisFirstTrack - thisStartSec
            thisTrackFraction = thisTrackDuration/(thisEndSec - thisStartSec)
            
            
        else:
            thisFirstTrack = -1
            thisFirstTrackDelay = -1
            thisTrackDuration = 0
            thisTrackFraction = 0
        
        
        trackingStatsList.append([thisTarget, thisState, thisSpeed, thisPosition, thisStartSec, thisEndSec, thisFirstTrack,
                            thisFirstTrackDelay, thisTrackFraction, thisTrackDuration])
        
    
    trackStatsDF = pd.DataFrame(trackingStatsList)
    with pd.ExcelWriter(trackingStatsFile, engine='openpyxl', mode='w') as writer:
        trackStatsDF.to_excel(writer, header = trackingStatsHeader, index = False)






