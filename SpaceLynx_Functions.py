import json
import time
import numpy as np
import pandas as pd
import Data_Classes as Classes
import Detection_Module as Detections
import assetstore
from assetstore.api import AssetServicesApi
from spacelynx.api import SchedulingApi


def Load_JSON_File(file_name: str) -> dict:
    # only needs to happen if you change the asset
    with open(file_name, encoding='utf-8') as f:
        data = json.load(f)
    return data


def Post_Asset(file_name: str, asset_config, global_args):

    asset_json = Load_JSON_File(file_name)
    with assetstore.ApiClient(asset_config) as api_client:
        api = AssetServicesApi(api_client=api_client)

        api.delete_asset_by_id(asset_json['asset_id'])
        api.add_asset(asset_spec_asset_spec=asset_json, **global_args)


def Get_Schedule_Results_by_ID(schedule_api: SchedulingApi, sch_id, global_args):

    resp = schedule_api.get_schedule_results_by_id(sch_id, **global_args)
    while not resp.done:
        time.sleep(.1)
        resp = schedule_api.get_schedule_results_by_id(sch_id, **global_args)
    return resp


def Get_Ops_from_Schedule(spacelynx_output):

    Op_List = []
    for thisAsset in spacelynx_output.schedule:
        thisAsset = spacelynx_output.schedule[thisAsset]
        for thisOp in thisAsset['ops']:
            Op_List.append(Classes.OutputData(thisOp))

    return Op_List


def Generate_Ops_Output(ops_data, scenario_data):

    i = 0

    Op_List = list()
    Output_Columns = ['Asset_Name', 'Payload_Name', 'Target_Name', 'Target_Latitude_deg', 'Target_Longitude_deg',
                      'Target_Altitude_km', 'Target_Priority', 'Target_Desc_A', 'Target_Desc_B', 'Start_Time', 'End_Time',
                      'Start_Time_sec', 'Dwell_sec', 'End_Time_sec', 'Start_Graze_deg', 'Start_Squint_deg',
                      'Start_InTrack_Dist_km', 'Start_XTrack_Dist_km', 'Asset_Roll_deg', 'Scan_X_deg', 'Scan_Y_deg', 'Radial_Vel_km_hr']

    for op in ops_data:

        i = i+1
        print(i)
        if op.geojson['type'] == 'Polygon':
            target_coord = json.loads(str(op.geojson['coordinates']))
            target_coord = np.array(target_coord)
            target_center = target_coord[0].mean(axis=0)
            op.Target_Latitude_deg = target_center[1]
            op.Target_Longitude_deg = target_center[0]
            try:
                op.Target_Altitude_km = target_center[2]
            except IndexError:
                op.Target_Altitude_km = 10
        else:
            op.Target_Latitude_deg = -9999
            op.Target_Longitude_deg = -9999
            op.Target_Altitude_km = -9999

        Target_Data_List = Detections.get_Ground_Truth_Data(scenario_data, op.Start_Time_sec)
        Target_Data = [Target_Data_List[i] for i in np.arange(len(Target_Data_List)) if Target_Data_List[i].Target_Name in op.Target_Name]
        if len(Target_Data) > 0:
            target_vel = np.array([Target_Data[0].Velocity_ECEF_X_km, Target_Data[0].Velocity_ECEF_Y_km, Target_Data[0].Velocity_ECEF_Z_km])
            target_pos = np.array([Target_Data[0].Position_ECEF_X_km, Target_Data[0].Position_ECEF_Y_km, Target_Data[0].Position_ECEF_Z_km])
            asset_pos = np.array(op.Pos_ECF)
            # target_vel_ned = navpy.ecef2ned(target_vel,op.Target_Latitude_deg, op.Target_Longitude_deg, op.Target_Altitude_km*1000)
            # asset_pos_lla = Geometry.ecf2lla(asset_pos)
            T2A = asset_pos/1000 - target_pos
            T2A_unit = T2A / np.linalg.norm(T2A)
            rad_velocity = np.dot(T2A_unit, target_vel)
            op.Radial_Vel_km_hr = rad_velocity
        else:
            op.Radial_Vel_km_hr = 0

        Op_Data_Dict = op.__dict__
        Op_List.append(Op_Data_Dict)

    Ops_df = pd.DataFrame(Op_List, columns=Output_Columns)

    return Ops_df
