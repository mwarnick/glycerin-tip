import datetime
import assetstore
import spacelynx
import pandas as pd
import numpy as np
from spacelynx.api import SchedulingApi, AccessGenerationApi
import SpaceLynx_Functions
import Ground_Truth_Module as Ground_Truth
import Detection_Module as Detections
import Tip_Generation_Module as Tips

# _________________________________Inputs_________________________________

Run_Directory = 'C:\\Users\\matthew.warnick\\Documents\\Glycerin\\Tip_Cue_Code_Docs\\Canada_Scenario\\'
Scenario_Description_File = 'Canadian_Scenario_Definition.xlsx'
Simulation_Setup_File = 'New_Roll_Priority.json'
Global_Args = {'_check_input_type': False, '_check_return_type': False}
Print_Interval_Files = True
tip_cue_interval_sec = 360
Track_Revisit_sec = 14
Break_Track_Length_min = 10
MDV_m_per_sec = 5
Processing_Delay_sec = 20

asset_config = assetstore.Configuration(host='http://localhost:9003')
spacelynx_config = spacelynx.Configuration(host='http://localhost:9000')

# ______________________________Run_Simulation______________________________

#  sim_set_up_dict is the excel run file converted to json.  Need to run excel2json to generate the file
sim_setup_dict = SpaceLynx_Functions.Load_JSON_File(Run_Directory + Simulation_Setup_File)
sim_start_time_str = sim_setup_dict['dates']['start_time']
sim_start_time_dt = datetime.datetime.strptime(sim_start_time_str, '%Y-%m-%dT%H:%M:%SZ')
sim_end_time_str = sim_setup_dict['dates']['end_time']
# sim_end_time_str = '2020-01-01T03:00:00Z'
sim_end_time_dt = datetime.datetime.strptime(sim_end_time_str, '%Y-%m-%dT%H:%M:%SZ')
interval_start_time_dt = sim_start_time_dt
sim_duration_dt = sim_end_time_dt - sim_start_time_dt
total_scheduling_intervals = np.ceil(sim_duration_dt.total_seconds()/tip_cue_interval_sec)
schedule_ID = 0
Tip_Target_Deck = {'id': 'Tips', 'targets': []}
sim_setup_dict['target_decks'].append(Tip_Target_Deck)

with spacelynx.ApiClient(spacelynx_config) as spacelynx_client:

    scheduling_api = SchedulingApi(api_client=spacelynx_client)
    accgen_api = AccessGenerationApi(api_client=spacelynx_client)
    accgen_api.delete_all_accgen_runs()
    scheduling_api.delete_all_schedules()
    Detections_All_df = pd.DataFrame()
    Ops_All_df = pd.DataFrame()
    Cumulative_Tip_List = list()
    print('Getting Ground Truth Data')
    Scenario_Data = Ground_Truth.Import_Scenario_Data(Run_Directory + Scenario_Description_File)

    while interval_start_time_dt < sim_end_time_dt:
        schedule_ID += 1
        sim_setup_dict['id'] = schedule_ID
        interval_end_time_dt = interval_start_time_dt + datetime.timedelta(seconds=tip_cue_interval_sec)
        interval_start_time_str = str(interval_start_time_dt.date()) + 'T' + str(interval_start_time_dt.time()) + 'Z'
        interval_end_time_str = str(interval_end_time_dt.date()) + 'T' + str(interval_end_time_dt.time()) + 'Z'
        sim_setup_dict['dates']['start_time'] = interval_start_time_str
        sim_setup_dict['dates']['end_time'] = interval_end_time_str

        print(f'\nInterval {schedule_ID} of {int(total_scheduling_intervals)}')
        print('Generating Schedule')
        scheduling_api.add_schedule(collection_run_spec_collection_run_spec=sim_setup_dict, **Global_Args)
        Sim_Result = SpaceLynx_Functions.Get_Schedule_Results_by_ID(scheduling_api, schedule_ID, Global_Args)
        Collection_Data = SpaceLynx_Functions.Get_Ops_from_Schedule(Sim_Result)
        Ops_df = SpaceLynx_Functions.Generate_Ops_Output(Collection_Data, Scenario_Data)
        Ops_All_df = Ops_All_df.append(Ops_df)

        print('Generating Detections')
        Detections_df = Detections.get_Detections(Collection_Data, Scenario_Data)
        Detections_All_df = Detections_All_df.append(Detections_df)

        print('Generating Tips')
        Tip_Target_Def = Tips.Generate_Tips(Detections_df, schedule_ID, MDV_m_per_sec, Track_Revisit_sec, Processing_Delay_sec, sim_end_time_str)
        for Tipped_Target in Tip_Target_Def:
            if Tipped_Target['description_b'] in Cumulative_Tip_List:
                Tipped_Target.update({'unique': False})
            else:
                Tipped_Target.update({'unique': True})
                Cumulative_Tip_List.append(Tipped_Target['description_b'])

        Ops_df_Movers = Ops_df[np.abs(Ops_df['Radial_Vel_km_hr']) > MDV_m_per_sec*3.6]
        unique_targets_collected = Ops_df_Movers.Target_Desc_B.unique()
        for tip in sim_setup_dict['target_decks'][-1]['targets']:
            if tip['description_b'] in unique_targets_collected:
                tip.update({'cont_track': True})
            else:
                tip.update({'cont_track': False})
                Cumulative_Tip_List.remove(tip['description_b'])

        Tip_Target_Def = [x for x in Tip_Target_Def if x['unique']]
        sim_setup_dict['target_decks'][-1]['targets'] = [x for x in sim_setup_dict['target_decks'][-1]['targets'] if x['cont_track']]
        # Tip_Targets = {'id': 'Tip_' + str(schedule_ID), 'targets': Tip_Target_Def}
        # sim_setup_dict['target_decks'].append(Tip_Targets)
        sim_setup_dict['target_decks'][-1]['targets'] = sim_setup_dict['target_decks'][-1]['targets'] + Tip_Target_Def

        interval_start_time_dt = interval_end_time_dt

        if Print_Interval_Files:
            Detections_df.to_csv(Run_Directory + 'Output_Files\\ID_' + str(schedule_ID) + '_Detections.csv', index=False)
            Ops_df.to_csv(Run_Directory + 'Output_Files\\ID_' + str(schedule_ID) + '_Ops.csv', index=False)

    Detections_All_df.to_csv(Run_Directory + 'Output_Files\\Detections_All.csv', index=False)
    Ops_All_df.to_csv(Run_Directory + 'Output_Files\\Ops_All.csv', index=False)
