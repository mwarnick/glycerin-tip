from dataclasses import dataclass, field
from typing import Optional, List, Dict
import datetime
from iso8601 import iso8601


@dataclass
class Frame:
    def __init__(self):
        pass

    x_axis: list
    y_axis: list
    z_axis: list


class Position:
    lat_deg: float
    lon_deg: float
    alt_km: float
    pos_ecf_km: list


class Attitude:
    reference_frame: str
    eci_to_body_rotation: list
    body_rates: list


class Geo_JSON:
    type: str
    coordinates: list


class Time:
    start: datetime.datetime
    end: datetime.datetime
    start_sec: float
    dwell_sec: float
    end_sec: float


class Asset_End_State:
    attitude: Attitude


class DownlinkData:
    Task_ID: str
    Start_Time: str
    End_Time: str
    Asset_Name: str
    Payload_Name: str
    Target_ID: str


class Asset:
    name: str
    position: Position
    frame_ecf: Frame
    euler_angles_ypr_deg: list
    eci2body: list
    rates_deg: list
    asset2payload: list
    slew_to_time_sec: float
    duty_period_start: datetime.datetime
    duty_used_sec: float


class Payload:
    name: str
    phenomenology: str
    frame_rel_asset: Frame
    euler_angles_ypr_deg: list
    rates_deg: list
    slew_to_time_sec: float
    end_attitude: Attitude


class Target:
    name: str
    priority: float
    description_a: str
    description_b: str
    geojson: Geo_JSON
    position: Position


class Geometry:
    graze_deg: float
    squint_deg: float
    nadir_deg: float
    earth_central_angle_deg: float
    depression_angle_deg: float
    incidence_angle_deg: float
    doppler_cone_angle_deg: float
    sun_elevation_angle_deg: float
    sun_position: Position
    moon_elevation_angle_deg: float
    cats_angle_deg: float
    catm_angle_deg: float
    altitude_km: float
    slant_range_km: float
    ground_range_km: float
    intrack_distance_km: float
    crosstrack_distance_km: float
    scan_x: float
    scan_y: float
    niirs: float


class Collection_Parameters:
    def __init__(self):
        pass

    number_scans_or_frames: int
    file_size_gbits: float
    ssdr_data_post_op_gbits: float
    cloudy_collect: str
    surface_temperature_deg_k: float
    target_area_collected_km: float
    area_collected_km: float
    downlinked_collect_ids: list
    percent_complete: float


class Op_Data:
    asset: Asset
    payload: Payload
    target: Target
    time: Time
    start_geometry: Geometry
    end_geometry: Geometry
    parameters: Collection_Parameters


class AccessData:
    Asset_ID: Optional[str]
    Access_Start_Time: str
    Access_End_Time: str
    Access_Start_Seconds_After_Sim_Epoch: Optional[float]
    Access_End_Seconds_After_Sim_Epoch: Optional[float]
    Access_Duration_Seconds: Optional[float]
    Asset_Name: str
    Description_A: Optional[str]
    Description_B: Optional[str]
    End_Geometry: Optional[Geometry]
    Payload_ID: Optional[str]
    Payload_Name: str
    Start_Geometry: Optional[Geometry]
    Target_Alt_km: Optional[float]
    Target_ID: str
    Target_Lat_deg: Optional[float]
    Target_Lon_deg: Optional[float]
    Target_Name: Optional[str]


class Detection:
    Time: datetime.datetime
    Time_sec: float
    Asset_Name: str
    Payload_Name: str
    Target_Name: str
    Target_Collected: bool
    Target_Visible: bool
    Target_Active: bool
    Target_Type: str
    Target_State: str
    Target_Latitude: float
    Target_Longitude: float
    Phenomenology: str
    Quality: float
    Detection_Type: str
    Total_Area_Collected: float
    PDetect: float
    FAR: float
    Radial_Velocity:  float


@dataclass
class ScheduleData:
    ops: List[Op_Data]
    end_state: object


@dataclass
class Response:
    id: Optional[str]
    simulation_name: Optional[str]
    done: bool
    error: bool
    schedule: Dict[str, ScheduleData] = field(default_factory=dict)
    accesses: List[AccessData] = field(default_factory=list)
    downlinks: List[DownlinkData] = field(default_factory=list)


class OutputData:
    def __init__(self, opdata_class):
        self.Asset_Name = opdata_class['asset_name']            # Changed here - all these changed to ['name']
        self.Payload_Name = opdata_class['payload_name']
        self.Target_Name = opdata_class['target_name']
        self.Target_Priority = opdata_class['target_priority']
        self.Target_Desc_A = opdata_class['description_a']
        self.Target_Desc_B = opdata_class['description_b']
        self.Phenomenology = opdata_class['phenomenology']
        self.Start_Time = iso8601.parse_date(opdata_class['start_collect_time'])        # Changed here check this iso8601.parse if it is doing what you want
        self.End_Time = iso8601.parse_date(opdata_class['end_collect_time'])
        self.Start_Time_sec = opdata_class['op_summary']['collect_start_seconds_after_sim_epoch']
        self.Dwell_sec = opdata_class['op_summary']['collect_time_sec']
        self.End_Time_sec = opdata_class['op_summary']['collect_start_seconds_after_sim_epoch'] + self.Dwell_sec
        self.Pos_ECF = opdata_class['start_data']['asset_position']['pos_ecf_km']
        self.Vel_ECF = opdata_class['start_data']['asset_position']['vel_ecf_kmpsec']
        self.geojson = opdata_class['geojson']
        self.NIIRS = opdata_class['start_geometry']['niirs']
        self.Target_Area_sq_km = opdata_class['op_summary']['target_area_collected_km']
        self.Total_Area_sq_km = opdata_class['op_summary']['area_collected_km']
        self.Start_Graze_deg = opdata_class['start_geometry']['graze_deg']
        self.Start_Squint_deg = opdata_class['start_geometry']['squint_deg']
        self.Start_InTrack_Dist_km = opdata_class['start_geometry']['intrack_distance_km']
        self.Start_XTrack_Dist_km = opdata_class['start_geometry']['crosstrack_distance_km']
        self.Asset_Roll_deg = opdata_class['start_data']['asset_euler_angles_ypr_deg'][2]
        self.Scan_X_deg = float(opdata_class['attributes']['scan_x_start'])
        self.Scan_Y_deg = float(opdata_class['attributes']['scan_y_start'])
